﻿using System.Collections.Generic;
using Iesi.Collections;

namespace WPF_NH.BL.Domain
{
    public class Department : Entity
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual Employee Chief { get; set; }
        public virtual Department HigherLevelDepartment { get; set; }
        public virtual ISet<Employee> Employees { get; set; }

        public Department()
        {
            this.Employees = new HashSet<Employee>();
        }

        public virtual void AddEmployee(Employee employee)
        {
            employee.Department = this;
            this.Employees.Add(employee);
        }

        public virtual void RemoveEmployee(Employee employee)
        {
            employee.Department = null;
            this.Employees.Remove(employee);
        }
    }
}