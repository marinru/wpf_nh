﻿namespace WPF_NH.BL.Domain
{
    public class Employee : Entity
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual Department Department { get; set; }
    }
}