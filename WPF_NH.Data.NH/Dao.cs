﻿using NHibernate;

using WPF_NH.BL;

namespace WPF_NH.Data.NH
{
    public class Dao<T> : IDao<T> where T: Entity
    {
        protected readonly ISessionFactory Factory;

        public Dao(ISessionFactory factory)
        {
            Factory = factory;
        }

        protected ISession CurrentSession
        {
            get { return Factory.GetCurrentSession(); }
        }
    }
}
