﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.Windsor;

namespace WPF_NH.Infrastructure.CW
{
    public class Wiring : IWiring
    {
        private IWindsorContainer container;

        public void Wire()
        {
            if (container != null)
            {
                Dewire();
            }
            container = new WindsorContainer();

            // TODO: IoC wiring
            throw new NotImplementedException();
        }

        /// <summary>
        /// Removes IoC wiring
        /// </summary>
        public void Dewire()
        {
            if (container != null)
            {
                container.Dispose();
            }                
        }
    }
}