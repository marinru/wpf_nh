﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF_NH.Infrastructure
{
    public interface IWiring
    {
        void Wire();
        void Dewire();
    }
}
